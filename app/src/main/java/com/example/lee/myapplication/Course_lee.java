package com.example.lee.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

public class Course_lee implements Parcelable {

    private int id;
    private String name;
    private String classNum;
    private String teacher;
    private int jieshu;
    private int weekBegin;//课程开始周
    private int weekEnd;//课程结束周
    private int flag;//判断单双周，0为无间断，1为单,2为双
    private String time;//上课时间
    private int day;//周几

    public Course_lee(int id, String name, String classNum, String teacher, int jieshu, int weekBegin, int weekEnd, int flag, String time, int day) {
        this.id = id;
        this.name = name;
        this.classNum = classNum;
        this.teacher = teacher;
        this.jieshu = jieshu;
        this.weekBegin = weekBegin;
        this.weekEnd = weekEnd;
        this.flag = flag;
        this.time = time;
        this.day = day;
    }

    public Course_lee(int id) {
        this.id = id;
    }
    public Course_lee() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public int getJieshu() {
        return jieshu;
    }

    public void setJieshu(int jieshu) {
        this.jieshu = jieshu;
    }

    public int getWeekBegin() {
        return weekBegin;
    }

    public void setWeekBegin(int weekBegin) {
        this.weekBegin = weekBegin;
    }

    public int getWeekEnd() {
        return weekEnd;
    }

    public void setWeekEnd(int weekEnd) {
        this.weekEnd = weekEnd;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.classNum);
        dest.writeString(this.teacher);
        dest.writeInt(this.jieshu);
        dest.writeInt(this.weekBegin);
        dest.writeInt(this.weekEnd);
        dest.writeInt(this.flag);
        dest.writeString(this.time);
        dest.writeInt(this.day);
    }

    protected Course_lee(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.classNum = in.readString();
        this.teacher = in.readString();
        this.jieshu = in.readInt();
        this.weekBegin = in.readInt();
        this.weekEnd = in.readInt();
        this.flag = in.readInt();
        this.time = in.readString();
        this.day = in.readInt();
    }

    public static final Creator<Course_lee> CREATOR = new Creator<Course_lee>() {
        @Override
        public Course_lee createFromParcel(Parcel source) {
            return new Course_lee(source);
        }

        @Override
        public Course_lee[] newArray(int size) {
            return new Course_lee[size];
        }
    };
}



