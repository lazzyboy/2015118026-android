package com.example.lee.myapplication;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DetailActivity extends AppCompatActivity {
    private MyDatabaseHelper dbHelper;//自定义数据库对象
    private SQLiteDatabase db;

    @Bind(R.id.back_detail)
    Button backDetail;
    @Bind(R.id.textView_detailTitle)
    TextView textViewDetailTitle;
    @Bind(R.id.set_detail)
    Button setDetail;
    @Bind(R.id.textView_className)
    TextView textViewClassName;
    @Bind(R.id.textView_classNum)
    TextView textViewClassNum;
    @Bind(R.id.textView_teacher)
    TextView textViewTeacher;
    @Bind(R.id.textView_jieshu)
    TextView textViewJieshu;
    @Bind(R.id.textView_classWeek)
    TextView textViewClassWeek;
    @Bind(R.id.textView_time)
    TextView textViewTime;

    EditText editName;
    EditText editClassNum;
    EditText editTeacher;
    EditText editJieshu;
    EditText editClassWeekBegin;
    EditText editClassWeekEnd;
    EditText editFlag;
    EditText editClassTime;
    private String id;
    private String name;
    private String num;
    private String teacher;
    private String jieshu;
    private String weekBegin;
    private String weekEnd;
    private String time;
    private String flag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();//隐藏标题栏
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);
        dbHelper = new MyDatabaseHelper(this, "CourseStore.db", null, 1);//上下文，数据库名字，第三个参数允许我们在查询数据的时候返回一个自定义的 Cursor，一般都是传入 null，第四个参数表示当前数据库的版本号
        db =dbHelper.getWritableDatabase();
        setData();//设置数据
        getCourse();//设置课程信息，

    }



    private void setData() {
        //Course_lee c = getIntent().getParcelableExtra("course");
        Intent intent = getIntent();
        id = intent.getStringExtra("TextViewId");
        Cursor cursor = db.rawQuery("select * from course where id=?",new String[] {id});

        if (cursor.moveToFirst()) {//cursor不为空
            do {
                //  遍历Cursor 对象，取出数据并set
                //int id=cursor.getInt(cursor.getColumnIndex("id"));
                name = cursor.getString(cursor.getColumnIndex("name"));
                num = cursor.getString(cursor.getColumnIndex("num"));
                teacher = cursor.getString(cursor.getColumnIndex("teacher"));
                jieshu =cursor.getString(cursor.getColumnIndex("jieshu"));
                weekBegin =cursor.getString(cursor.getColumnIndex("weekBegin"));
                weekEnd =cursor.getString(cursor.getColumnIndex("weekEnd"));
                flag=cursor.getString(cursor.getColumnIndex("flag"));
                time=cursor.getString(cursor.getColumnIndex("time"));

            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    private void getCourse() {
        textViewDetailTitle.setText(name);
        textViewClassName.setText(name);
        textViewClassNum.setText(num);
        textViewTeacher.setText(teacher);
        textViewJieshu.setText(jieshu);
        textViewClassWeek.setText(weekBegin + "-" + weekEnd + " (" + flag + ")");
        textViewTime.setText(time);
    }

    private void saveData(String id) {
        Log.e("tag",editName.getText().toString());
        Log.e("tag", id);
        db.execSQL("update course set name = ? where id = ?", new String[]{editName.getText().toString(), id});
        db.execSQL("update course set num = ? where id = ?", new String[]{editClassNum.getText().toString(), id});
        db.execSQL("update course set teacher = ? where id = ?", new String[]{editTeacher.getText().toString(), id});
        db.execSQL("update course set jieshu = ? where id = ?", new String[]{editJieshu.getText().toString(), id});
        db.execSQL("update course set weekBegin = ? where id = ?", new String[]{editClassWeekBegin.getText().toString(), id});
        db.execSQL("update course set weekEnd = ? where id = ?", new String[]{editClassWeekEnd.getText().toString(), id});
        db.execSQL("update course set flag = ? where id = ?", new String[]{editFlag.getText().toString(), id});
        db.execSQL("update course set time = ? where id = ?", new String[]{editClassTime.getText().toString(), id});
    }


    private void updateUI() {
        textViewClassNum.setText(editClassNum.getText());
        textViewTime.setText(editClassTime.getText());
        textViewClassWeek.setText(editClassWeekBegin.getText()+"-"+editClassWeekEnd.getText()+" ("+flag+")");
        textViewJieshu.setText(editJieshu.getText());
        textViewTeacher.setText(editTeacher.getText());
        textViewClassName.setText(editName.getText());
    }


    @OnClick({R.id.back_detail, R.id.set_detail})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_detail:
                finish();
                break;
            case R.id.set_detail:
                //弹出一个自定义修改框，里面显示课程信息，有确认和取消，确认后修改存储数据到 SharedPreferences并显示数据
                Builder builder = new Builder(this);
                LinearLayout Form = (LinearLayout) getLayoutInflater().inflate(R.layout.edit_detail, null);
                builder.setView(Form);
                //加载数据到editView
                editName= (EditText) Form.findViewById(R.id.edit_name);//注意要加Form，因为this的layout文件是外部的activity_detail而不是警告框的layout
                editClassNum= (EditText) Form.findViewById(R.id.edit_classNum);
                editTeacher= (EditText) Form.findViewById(R.id.edit_teacher);
                editJieshu= (EditText) Form.findViewById(R.id.edit_jieshu);
                editClassWeekBegin= (EditText) Form.findViewById(R.id.edit_classWeekBegin);
                editClassWeekEnd= (EditText) Form.findViewById(R.id.edit_classWeekEnd);
                editFlag=(EditText) Form.findViewById(R.id.edit_flag);
                editClassTime= (EditText) Form.findViewById(R.id.edit_classTime);

                editName.setText(name);
                editClassTime.setText(time);
                editClassWeekBegin.setText(weekBegin);
                editClassWeekEnd.setText(weekEnd);
                editFlag.setText(flag);
                editJieshu.setText(jieshu);
                editClassNum.setText(num);
                editTeacher.setText(teacher);

                builder.setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveData(id);
                        updateUI();
                    }
                });

                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Log.d("cancel", "cancel");
                    }
                });
                builder.show();

                break;
        }
    }


}
//SharedPreferences demo
//        SharedPreferences.Editor editor = getSharedPreferences("classData", MODE_PRIVATE).edit();
//        //if (!editName.getText().equals(""))
//        editor.putString("name", editName.getText().toString());
//        editor.putString("classNum", editClassNum.getText().toString());
//        editor.putString("teacher", editTeacher.getText().toString());
//        editor.putString("jieshu", editJieshu.getText().toString());
//        editor.putString("classWeek", editClassWeek.getText().toString());
//        editor.putString("classTime", editClassTime.getText().toString());
//        editor.apply();//本来是commit