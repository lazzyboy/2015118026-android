package com.example.lee.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;


public class MyDatabaseHelper extends SQLiteOpenHelper {

    //sqlite数据库类型：integer 表示整型，real 表示浮点型，text 表示文本类型，blob 表示二进制类型
    public static final String CREATE_COURSE="create table course("
            +"id integer primary key, "//绑定的是Textview的id
            + "name text, "
            + "num text, "
            + "teacher text, "
            + "jieshu integer,"
            + "weekBegin integer,"
            + "weekEnd integer,"
            + "flag integer,"
            + "time text,"
            + "day integer)";



    public MyDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_COURSE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
