package com.example.lee.myapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Vector;

//import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private MyDatabaseHelper dbHelper;//自定义数据库对象
    private Vector<Course_lee> courseList= new Vector<Course_lee>();//注意初始化
    private TextView tv_mainActivity;
    private Cursor cursor;

    //获取所有id
//    @Bind(R.id.back)
//    Button back;
//    @Bind(R.id.week)
//    TextView week;
//    @Bind(R.id.set)
//    Button set;
//    @Bind(R.id.mon)
//    TextView mon;
//    @Bind(R.id.tue)
//    TextView tue;
//    @Bind(R.id.wed)
//    TextView wed;
//    @Bind(R.id.thur)
//    TextView thur;
//    @Bind(R.id.fri)
//    TextView fri;
//    @Bind(R.id.sta)
//    TextView sta;
//    @Bind(R.id.sun)
//    TextView sun;
//    @Bind(R.id.textView1_1)
//    TextView textView11;
//    @Bind(R.id.textView1_2)
//    TextView textView12;
//    @Bind(R.id.textView1_3)
//    TextView textView13;
//    @Bind(R.id.textView1_4)
//    TextView textView14;
//    @Bind(R.id.textView1_5)
//    TextView textView15;
//    @Bind(R.id.textView1_6)
//    TextView textView16;
//    @Bind(R.id.textView1_7)
//    TextView textView17;
//    @Bind(R.id.textView2_1)
//    TextView textView21;
//    @Bind(R.id.textView2_2)
//    TextView textView22;
//    @Bind(R.id.textView2_3)
//    TextView textView23;
//    @Bind(R.id.textView2_4)
//    TextView textView24;
//    @Bind(R.id.textView2_5)
//    TextView textView25;
//    @Bind(R.id.textView2_6)
//    TextView textView26;
//    @Bind(R.id.textView2_7)
//    TextView textView27;
//    @Bind(R.id.textView3_1)
//    TextView textView31;
//    @Bind(R.id.textView3_2)
//    TextView textView32;
//    @Bind(R.id.textView3_3)
//    TextView textView33;
//    @Bind(R.id.textView3_4)
//    TextView textView34;
//    @Bind(R.id.textView3_5)
//    TextView textView35;
//    @Bind(R.id.textView3_6)
//    TextView textView36;
//    @Bind(R.id.textView3_7)
//    TextView textView37;
//    @Bind(R.id.textView4_1)
//    TextView textView41;
//    @Bind(R.id.textView4_2)
//    TextView textView42;
//    @Bind(R.id.textView4_3)
//    TextView textView43;
//    @Bind(R.id.textView4_4)
//    TextView textView44;
//    @Bind(R.id.textView4_5)
//    TextView textView45;
//    @Bind(R.id.textView4_6)
//    TextView textView46;
//    @Bind(R.id.textView4_7)
//    TextView textView47;
//    @Bind(R.id.textView5_1)
//    TextView textView51;
//    @Bind(R.id.textView5_2)
//    TextView textView52;
//    @Bind(R.id.textView5_3)
//    TextView textView53;
//    @Bind(R.id.textView5_4)
//    TextView textView54;
//    @Bind(R.id.textView5_5)
//    TextView textView55;
//    @Bind(R.id.textView5_6)
//    TextView textView56;
//    @Bind(R.id.textView5_7)
//    TextView textView57;
//    @Bind(R.id.textView6_1)
//    TextView textView61;
//    @Bind(R.id.textView6_2)
//    TextView textView62;
//    @Bind(R.id.textView6_3)
//    TextView textView63;
//    @Bind(R.id.textView6_4)
//    TextView textView64;
//    @Bind(R.id.textView6_5)
//    TextView textView65;
//    @Bind(R.id.textView6_6)
//    TextView textView66;
//    @Bind(R.id.textView6_7)
//    TextView textView67;
//    @Bind(R.id.textView7_1)
//    TextView textView71;
//    @Bind(R.id.textView7_2)
//    TextView textView72;
//    @Bind(R.id.textView7_3)
//    TextView textView73;
//    @Bind(R.id.textView7_4)
//    TextView textView74;
//    @Bind(R.id.textView7_5)
//    TextView textView75;
//    @Bind(R.id.textView7_6)
//    TextView textView76;
//    @Bind(R.id.textView7_7)
//    TextView textView77;
//    @Bind(R.id.textView8_1)
//    TextView textView81;
//    @Bind(R.id.textView8_2)
//    TextView textView82;
//    @Bind(R.id.textView8_3)
//    TextView textView83;
//    @Bind(R.id.textView8_4)
//    TextView textView84;
//    @Bind(R.id.textView8_5)
//    TextView textView85;
//    @Bind(R.id.textView8_6)
//    TextView textView86;
//    @Bind(R.id.textView8_7)
//    TextView textView87;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lee);
        //getSupportActionBar().hide();
        ButterKnife.bind(this);
//        Calendar c=Calendar.getInstance();
//        int day_of_week=c.get(Calendar.DAY_OF_WEEK);
        //获取数据库，没有就创建一个
        dbHelper = new MyDatabaseHelper(this, "CourseStore.db", null, 1);//上下文，数据库名字，第三个参数允许我们在查询数据的时候返回一个自定义的 Cursor，一般都是传入 null，第四个参数表示当前数据库的版本号
        SQLiteDatabase db =dbHelper.getWritableDatabase();
        cursor = db.rawQuery("select * from course", null);
        first_init(db, cursor);
        init(cursor);

    }

    @Override
    protected void onResume() {
        super.onResume();
        flashUI();//刷新UI
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void flashUI() {
        //重新在数据库提取值
        SQLiteDatabase db =dbHelper.getWritableDatabase();
        cursor = db.rawQuery("select * from course", null);
        if (cursor.moveToFirst()) {//cursor不为空
            do {
                //  遍历Cursor 对象，取出数据并set
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String num = cursor.getString(cursor.getColumnIndex("num"));
                String teacher = cursor.getString(cursor.getColumnIndex("teacher"));
                int jieshu = cursor.getInt(cursor.getColumnIndex("jieshu"));
                int weekBegin = cursor.getInt(cursor.getColumnIndex("weekBegin"));
                int weekEnd = cursor.getInt(cursor.getColumnIndex("weekEnd"));
                int flag = cursor.getInt(cursor.getColumnIndex("flag"));
                String time = cursor.getString(cursor.getColumnIndex("time"));
                int day = cursor.getInt(cursor.getColumnIndex("day"));

                //主页面显示课程名和教室号
                tv_mainActivity = (TextView) findViewById(id);
                tv_mainActivity.setText(name + "\n" + num);

                if (!courseList.isEmpty()) {//对象数组course不为空
                    for (int i = 0; i < courseList.size(); i++) {
                        //找到对应id的课程信息
                        if (courseList.elementAt(i).getId() == id) {
                            //更新对应的courseList对象数组所存储的信息
                            courseList.elementAt(i).setName(name);
                            courseList.elementAt(i).setClassNum(num);
                            courseList.elementAt(i).setTeacher(teacher);
                            courseList.elementAt(i).setJieshu(jieshu);
                            courseList.elementAt(i).setWeekBegin(weekBegin);
                            courseList.elementAt(i).setWeekEnd(weekEnd);
                            courseList.elementAt(i).setFlag(flag);
                            courseList.elementAt(i).setTime(time);
                            courseList.elementAt(i).setDay(day);
                            break;//找到后退出循环
                        }
                    }
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    private void first_init(SQLiteDatabase db,Cursor cursor) {
        if (!cursor.moveToFirst()) {//查询结果是否为空
            //为空时，初始化course对象数组，使其值为空
            courseList.add(new Course_lee(R.id.textView1_1));//之后要编写一个方便的实例化对象的方法
            courseList.add(new Course_lee(R.id.textView1_2));
            courseList.add(new Course_lee(R.id.textView1_3));
            courseList.add(new Course_lee(R.id.textView1_4));
            courseList.add(new Course_lee(R.id.textView1_5));
            courseList.add(new Course_lee(R.id.textView1_6));
            courseList.add(new Course_lee(R.id.textView1_7));
            courseList.add(new Course_lee(R.id.textView2_1));
            courseList.add(new Course_lee(R.id.textView2_2));
            courseList.add(new Course_lee(R.id.textView2_3));
            courseList.add(new Course_lee(R.id.textView2_4));
            courseList.add(new Course_lee(R.id.textView2_5));
            courseList.add(new Course_lee(R.id.textView2_6));
            courseList.add(new Course_lee(R.id.textView2_7));
            courseList.add(new Course_lee(R.id.textView3_1));
            courseList.add(new Course_lee(R.id.textView3_2));
            courseList.add(new Course_lee(R.id.textView3_3));
            courseList.add(new Course_lee(R.id.textView3_4));
            courseList.add(new Course_lee(R.id.textView3_5));
            courseList.add(new Course_lee(R.id.textView3_6));
            courseList.add(new Course_lee(R.id.textView3_7));
            courseList.add(new Course_lee(R.id.textView4_1));
            courseList.add(new Course_lee(R.id.textView4_2));
            courseList.add(new Course_lee(R.id.textView4_3));
            courseList.add(new Course_lee(R.id.textView4_4));
            courseList.add(new Course_lee(R.id.textView4_5));
            courseList.add(new Course_lee(R.id.textView4_6));
            courseList.add(new Course_lee(R.id.textView4_7));
            courseList.add(new Course_lee(R.id.textView5_1));
            courseList.add(new Course_lee(R.id.textView5_2));
            courseList.add(new Course_lee(R.id.textView5_3));
            courseList.add(new Course_lee(R.id.textView5_4));
            courseList.add(new Course_lee(R.id.textView5_5));
            courseList.add(new Course_lee(R.id.textView5_6));
            courseList.add(new Course_lee(R.id.textView5_7));
            courseList.add(new Course_lee(R.id.textView6_1));
            courseList.add(new Course_lee(R.id.textView6_2));
            courseList.add(new Course_lee(R.id.textView6_3));
            courseList.add(new Course_lee(R.id.textView6_4));
            courseList.add(new Course_lee(R.id.textView6_5));
            courseList.add(new Course_lee(R.id.textView6_6));
            courseList.add(new Course_lee(R.id.textView6_7));
            courseList.add(new Course_lee(R.id.textView7_1));
            courseList.add(new Course_lee(R.id.textView7_2));
            courseList.add(new Course_lee(R.id.textView7_3));
            courseList.add(new Course_lee(R.id.textView7_4));
            courseList.add(new Course_lee(R.id.textView7_5));
            courseList.add(new Course_lee(R.id.textView7_6));
            courseList.add(new Course_lee(R.id.textView7_7));
            courseList.add(new Course_lee(R.id.textView8_1));
            courseList.add(new Course_lee(R.id.textView8_2));
            courseList.add(new Course_lee(R.id.textView8_3));
            courseList.add(new Course_lee(R.id.textView8_4));
            courseList.add(new Course_lee(R.id.textView8_5));
            courseList.add(new Course_lee(R.id.textView8_6));
            courseList.add(new Course_lee(R.id.textView8_7));

            //创建一个包含当前courselist数据的数据库的表
            insertPrimaryTable(courseList,db);
        }

    }

    private void insertPrimaryTable(Vector<Course_lee> courseList,SQLiteDatabase db) {
        int id;
        for (int i=0;i<courseList.size();i++){
            //获取vector数据
            id=courseList.elementAt(i).getId();
            ContentValues values = new ContentValues();
            //  开始组装第一条数据
            values.put("id", id);
            db.insert("course", null, values);
            values.clear();
        }
    }


    private void setWeek() {
        //设置周数
    }

    private void init(Cursor cursor) {
        //启动时基本配置
        //Cursor cursor = db.rawQuery("select * from course where day=? and time=?", new String[] {"1",""});
        //Cursor cursor = db.rawQuery("select * from course",null);

        if (cursor.moveToFirst()) {//cursor不为空
            do {
                //  遍历Cursor 对象，取出数据并set
                int id=cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String num = cursor.getString(cursor.getColumnIndex("num"));
//                String teacher = cursor.getString(cursor.getColumnIndex("teacher"));
//                int jieshu =cursor.getInt(cursor.getColumnIndex("jieshu"));
//                int weekBegin =cursor.getInt(cursor.getColumnIndex("weekBegin"));
//                int weekEnd =cursor.getInt(cursor.getColumnIndex("weekEnd"));
//                int flag=cursor.getInt(cursor.getColumnIndex("flag"));
//                String time=cursor.getString(cursor.getColumnIndex("time"));
//                int day = cursor.getInt(cursor.getColumnIndex("day"));

                //主页面显示课程名和教室号
                tv_mainActivity= (TextView) findViewById(id);
                tv_mainActivity.setText(name+"\n"+num);

//                if (!courseList.isEmpty()){//对象数组course不为空
//                    for (int i=0;i<courseList.size();i++){
//                        //找到对应id的课程信息
//                        if(courseList.elementAt(i).getId()==id){
//                            //更新对应的courseList对象数组所存储的信息
//                            courseList.elementAt(i).setName(name);
//                            courseList.elementAt(i).setClassNum(num);
//                            courseList.elementAt(i).setTeacher(teacher);
//                            courseList.elementAt(i).setJieshu(jieshu);
//                            courseList.elementAt(i).setWeekBegin(weekBegin);
//                            courseList.elementAt(i).setWeekEnd(weekEnd);
//                            courseList.elementAt(i).setFlag(flag);
//                            courseList.elementAt(i).setTime(time);
//                            courseList.elementAt(i).setDay(day);
//                            break;//找到后退出循环
////                        }
//                    }
//                }
            } while (cursor.moveToNext());
            cursor.close();
        }



    }


    void highlight(){
        //高亮提示当前周要上的课
    }

    void updateTable(String name,String num){
        //更新表格内容

    }

    private void enterDetail(int id) {
        String tmpId=String.valueOf(id);
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        //intent.putExtra("course", course);
        intent.putExtra("TextViewId",tmpId);
        Log.e("MainTVID", String.valueOf(id));
        startActivity(intent);
    }



    //为textView和btn分别注册监听器
    @OnClick({R.id.back, R.id.set, R.id.textView1_1, R.id.textView1_2, R.id.textView1_3, R.id.textView1_4, R.id.textView1_5, R.id.textView1_6, R.id.textView1_7, R.id.textView2_1, R.id.textView2_2, R.id.textView2_3, R.id.textView2_4, R.id.textView2_5, R.id.textView2_6, R.id.textView2_7, R.id.textView3_1, R.id.textView3_2, R.id.textView3_3, R.id.textView3_4, R.id.textView3_5, R.id.textView3_6, R.id.textView3_7, R.id.textView4_1, R.id.textView4_2, R.id.textView4_3, R.id.textView4_4, R.id.textView4_5, R.id.textView4_6, R.id.textView4_7, R.id.textView5_1, R.id.textView5_2, R.id.textView5_3, R.id.textView5_4, R.id.textView5_5, R.id.textView5_6, R.id.textView5_7, R.id.textView6_1, R.id.textView6_2, R.id.textView6_3, R.id.textView6_4, R.id.textView6_5, R.id.textView6_6, R.id.textView6_7, R.id.textView7_1, R.id.textView7_2, R.id.textView7_3, R.id.textView7_4, R.id.textView7_5, R.id.textView7_6, R.id.textView7_7, R.id.textView8_1, R.id.textView8_2, R.id.textView8_3, R.id.textView8_4, R.id.textView8_5, R.id.textView8_6, R.id.textView8_7})
    public void onClick(View view) {
        int id=view.getId();
        Log.e("onclick", String.valueOf(id));
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.set:
                //弹出一个有TextView的警告框，确认后修改当前周并且刷新当前周有的课
                break;
//            case R.id.textView1_1:
//                course=findCorrectCourse(R.id.textView1_1);//找到符合这个id的课程
//                enterDetail(course);//跳转页面，把数据显示在下一个activitiy
//                break;
            default:
//                Course_lee c=new Course_lee();//注意初始化
//                for (int i=0;i<courseList.size();i++) {
//                    //找到对应id的课程信息
//                    if (courseList.elementAt(i).getId() == id) {
//                        c=courseList.elementAt(i);
//                    }
//                    else c= null;
//                }
//                enterDetail(c,id);//跳转页面，把数据显示在下一个activitiy
                enterDetail(id);
                break;
        }
    }

//    private Course_lee findCorrectCourse(int tv) {
//        Course_lee course;
//        for (int i=0;i<courseList.size();i++) {
//            //找到对应id的课程信息
//            if (courseList.elementAt(i).getId() == tv) {
//                course=courseList.elementAt(i);
//                return course;
//            }
//        }
//        return null;
//    }

}


